const mongoose = require('mongoose');
const config = require('./config');
const {nanoid} = require('nanoid');
const User = require("./models/User");
const Photo = require("./models/Photo");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);
  const collections = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collections) {
    await mongoose.connection.dropCollection(coll.name);
  }

  const [user, user2, user3] = await User.create({
    email: 'user@mail.ru',
    password: '123',
    avatar: 'user.jpg',
    displayName: 'Jack Doe',
    token: nanoid(),
  }, {
    email: 'user2@mail.ru',
    password: '123',
    avatar: 'user2.jpg',
    displayName: 'Oliver Smith',
    token: nanoid(),
  }, {
    email: 'user3@mail.ru',
    password: '123',
    avatar: 'user3.jpg',
    displayName: 'Olivia Martin',
    token: nanoid(),
  });

  await Photo.create({
    title: 'Children',
    user: user,
    image: 'children.jpg'
  }, {
    title: 'Coast',
    user: user,
    image: 'coast.jpg'
  }, {
    title: 'Fish',
    user: user,
    image: 'fish.jpg'
  }, {
    title: 'Paris',
    user: user2,
    image: 'paris.jpg'
  }, {
    title: 'Portrait',
    user: user2,
    image: 'portrait.jpg'
  }, {
    title: 'Stones',
    user: user3,
    image: 'stones.jpg'
  }, {
    title: 'Sunset',
    user: user3,
    image: 'sunset.jpg'
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));