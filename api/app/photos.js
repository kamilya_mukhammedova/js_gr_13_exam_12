const express = require('express');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const Photo = require("../models/Photo");
const auth = require("../middleware/auth");
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    if(req.query.user) {
      const userPhotos = await Photo.find({user: req.query.user}).populate('user', 'displayName');
      return res.send(userPhotos);
    }
    const allPhotos = await Photo.find().populate('user', 'displayName');
    return res.send(allPhotos);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', auth, async (req, res, next) => {
  try {
   const photo = await Photo.findById(req.params.id);
   if(!photo) {
     return res.status(404).send({message: `Photo with id ${req.params.id} not found!`});
   }
    await Photo.deleteOne({_id: req.params.id});
    return res.send({message: `Photo with id ${req.params.id} has been removed!`});
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.title || !req.file) {
      return res.status(400).send({message: 'Title and image are required'});
    }
    const newPhoto = new Photo({
      title: req.body.title,
      user: req.user._id,
      image: req.file.filename
    });
    await newPhoto.save();
    return res.send(newPhoto);
  } catch (e) {
    next(e);
  }
});

module.exports = router;