export class Photo {
  constructor(
    public id: string,
    public title: string,
    public user: PhotoUserData,
    public image: string
  ) {}
}

export interface PhotoUserData {
  _id: string,
  displayName: string
}

export interface ApiPhotoData {
  _id: string,
  title: string,
  user: PhotoUserData,
  image: string
}

export interface PhotoData {
  [key: string]: any,
  title: string,
  image: File,
}
