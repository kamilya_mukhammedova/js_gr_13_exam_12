export interface ModalDialogData {
  image: string,
  title: string,
}
