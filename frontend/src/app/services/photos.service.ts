import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiPhotoData, Photo, PhotoData } from '../models/photo.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {

  constructor(private http: HttpClient) {}

  getAllPhotos() {
    return this.http.get<ApiPhotoData[]>(environment.apiUrl + '/photos').pipe(
      map(response => {
        return response.map(photoData => {
          return new Photo(
            photoData._id,
            photoData.title,
            photoData.user,
            photoData.image
          );
        });
      })
    );
  }

  getUserPhotos(userId: string) {
    return this.http.get<ApiPhotoData[]>(environment.apiUrl + `/photos?user=${userId}`).pipe(
      map(response => {
        return response.map(photoData => {
          return new Photo(
            photoData._id,
            photoData.title,
            photoData.user,
            photoData.image
          );
        });
      })
    );
  }

  removePhoto(photoId: string) {
    return this.http.delete<{message: string}>(environment.apiUrl + `/photos/${photoId}`);
  }

  createNewPhoto(photoData: PhotoData) {
    const formData = new FormData();
    Object.keys(photoData).forEach(key => {
      if (photoData[key] !== null) {
        formData.append(key, photoData[key]);
      }
    });
    return this.http.post<ApiPhotoData>(environment.apiUrl + '/photos', formData);
  }
}
