import { PhotosState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createPhotoFailure,
  createPhotoRequest,
  createPhotoSuccess,
  deletePhotoFailure,
  deletePhotoRequest,
  deletePhotoSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess,
  fetchUserPhotosFailure,
  fetchUserPhotosRequest,
  fetchUserPhotosSuccess
} from './photos.actions';

const initialState: PhotosState = {
  photos: [],
  fetchLoading: false,
  fetchError:  null,
  fetchUserPhotoLoading: false,
  fetchUserPhotoError: null,
  removeLoading: false,
  removeError: null,
  createLoading: false,
  createError: null,
};

export const photosReducer = createReducer(
  initialState,
  on(fetchPhotosRequest, state => ({...state, fetchLoading: true})),
  on(fetchPhotosSuccess, (state, {photos}) => ({
    ...state,
    fetchLoading: false,
    photos
  })),
  on(fetchPhotosFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(fetchUserPhotosRequest, state => ({
    ...state,
    fetchUserPhotoLoading: true,
  })),
  on(fetchUserPhotosSuccess, (state, {photos}) => ({
    ...state,
    fetchUserPhotoLoading: false,
    photos
  })),
  on(fetchUserPhotosFailure, (state, {error}) => ({
    ...state,
    fetchUserPhotoLoading: false,
    fetchUserPhotoError: error
  })),
  on(deletePhotoRequest, state => ({
    ...state,
    removeLoading: true,
  })),
  on(deletePhotoSuccess, state => ({
    ...state,
    removeLoading: false,
  })),
  on(deletePhotoFailure, (state, {error}) => ({
    ...state,
    removeLoading: false,
    removeError: error
  })),
  on(createPhotoRequest, state => ({...state, createLoading: true})),
  on(createPhotoSuccess, state => ({...state, createLoading: false})),
  on(createPhotoFailure, (state, {error}) => ({
    ...state,
    createLoading: false,
    createError: error,
  })),
);
