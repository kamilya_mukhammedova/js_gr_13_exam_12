import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PhotosService } from '../services/photos.service';
import {
  createPhotoFailure,
  createPhotoRequest,
  createPhotoSuccess,
  deletePhotoFailure,
  deletePhotoRequest,
  deletePhotoSuccess,
  fetchPhotosFailure,
  fetchPhotosRequest,
  fetchPhotosSuccess,
  fetchUserPhotosFailure,
  fetchUserPhotosRequest,
  fetchUserPhotosSuccess
} from './photos.actions';
import { catchError, map, mergeMap, Observable, of, tap } from 'rxjs';
import { HelpersService } from '../services/helpers.service';
import { Store } from '@ngrx/store';
import { AppState } from './types';
import { User } from '../models/user.model';
import { Router } from '@angular/router';

@Injectable()
export class PhotosEffects {
  user: Observable<null | User>;
  userId = '';

  constructor(
    private actions: Actions,
    private photosService: PhotosService,
    private helpers: HelpersService,
    private store: Store<AppState>,
    private router: Router
  ) {
    this.user = store.select(state => state.users.user);
  }

  fetchAllPhotos = createEffect(() => this.actions.pipe(
    ofType(fetchPhotosRequest),
    mergeMap(() => this.photosService.getAllPhotos().pipe(
      map(photos => fetchPhotosSuccess({photos})),
      catchError(() => of(fetchPhotosFailure({
        error: 'Something went wrong with photos uploading!'
      })))
    ))
  ));

  fetchUserPhotos = createEffect(() => this.actions.pipe(
    ofType(fetchUserPhotosRequest),
    mergeMap(({userId}) => this.photosService.getUserPhotos(userId).pipe(
      map(photos => fetchUserPhotosSuccess({photos})),
      catchError(() => of(fetchUserPhotosFailure({
        error: 'Something went wrong with user photos uploading!'
      })))
    ))
  ));

  removePhoto = createEffect(() => this.actions.pipe(
    ofType(deletePhotoRequest),
    mergeMap(({photoId}) => this.photosService.removePhoto(photoId).pipe(
      map(() => deletePhotoSuccess()),
      tap(() => {
        const userId = this.getCurrentUserId();
        this.store.dispatch(fetchUserPhotosRequest({userId}));
        this.helpers.openSnackbar('Removing photo successful');
      }),
      catchError(() => of(deletePhotoFailure({
        error: 'Something went wrong! Can\'t remove photo!'
      })))
    ))
  ));

  createPhoto = createEffect(() => this.actions.pipe(
    ofType(createPhotoRequest),
    mergeMap(({photoData}) => this.photosService.createNewPhoto(photoData).pipe(
      map(() => createPhotoSuccess()),
      tap(() => {
        const userId = this.getCurrentUserId();
        void this.router.navigate(['/photos/user/', userId]);
        this.helpers.openSnackbar('Adding photo successful!');
      }),
      catchError(() => of(createPhotoFailure({error: 'Wrong data of photo!'})))
    ))
  ));

  getCurrentUserId(): string {
    this.user.subscribe(user => {
       this.userId = user!._id;
    });
    return this.userId;
  }
}
