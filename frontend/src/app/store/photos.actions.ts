import { createAction, props } from '@ngrx/store';
import { Photo, PhotoData } from '../models/photo.model';

export const fetchPhotosRequest = createAction(
  '[Photos] Fetch Request'
);
export const fetchPhotosSuccess = createAction(
  '[Photos] Fetch Success',
  props<{ photos: Photo[] }>()
);
export const fetchPhotosFailure = createAction(
  '[Photos] Fetch Failure',
  props<{ error: string }>()
);
export const fetchUserPhotosRequest = createAction(
  '[User Photos] Fetch Request',
  props<{ userId: string }>()
);
export const fetchUserPhotosSuccess = createAction(
  '[User Photos] Fetch Success',
  props<{ photos: Photo[]}>()
);
export const fetchUserPhotosFailure = createAction(
  '[User Photos] Fetch Failure',
  props<{ error: string }>()
);
export const deletePhotoRequest = createAction(
  '[Photo] Delete Request',
  props<{photoId: string}>()
);
export const deletePhotoSuccess = createAction(
  '[Photo] Delete Success'
);
export const deletePhotoFailure = createAction(
  '[Photo] Delete Failure',
  props<{error: string}>()
);
export const createPhotoRequest = createAction(
  '[Photo] Create Request',
  props<{ photoData: PhotoData }>()
);
export const createPhotoSuccess = createAction(
  '[Photo] Create Success'
);
export const createPhotoFailure = createAction(
  '[Photo] Create Failure',
  props<{ error: string }>()
);
