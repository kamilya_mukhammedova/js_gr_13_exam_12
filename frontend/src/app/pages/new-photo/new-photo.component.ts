import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable } from 'rxjs';
import { PhotoData } from '../../models/photo.model';
import { createPhotoRequest } from '../../store/photos.actions';

@Component({
  selector: 'app-new-photo',
  templateUrl: './new-photo.component.html',
  styleUrls: ['./new-photo.component.sass']
})
export class NewPhotoComponent {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.photos.createLoading);
    this.error = store.select(state => state. photos.createError);
  }

  onSubmit() {
    const photoData: PhotoData = this.form.value;
    this.store.dispatch(createPhotoRequest({photoData}));
  }
}
