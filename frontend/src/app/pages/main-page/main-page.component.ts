import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Photo } from '../../models/photo.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchPhotosRequest } from '../../store/photos.actions';
import { MatDialog } from '@angular/material/dialog';
import { ModalWindowComponent } from '../../ui/modal-window/modal-window.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.sass']
})
export class MainPageComponent implements OnInit {
  photos: Observable<Photo[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;

  constructor(
    private store: Store<AppState>,
    public dialog: MatDialog
    ) {
    this.photos = store.select(state => state.photos.photos);
    this.loading = store.select(state => state.photos.fetchLoading);
    this.error = store.select(state => state.photos.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPhotosRequest());
  }

  openModalWindow(photoImage: string, photoTitle: string) {
    const dialogRef = this.dialog.open(ModalWindowComponent, {
      width: '60%',
      height: '80%',
      data: {image: photoImage, title: photoTitle},
    });
  }
}
