import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { Observable, Subscription } from 'rxjs';
import { Photo } from '../../models/photo.model';
import { ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ModalWindowComponent } from '../../ui/modal-window/modal-window.component';
import { deletePhotoRequest, fetchUserPhotosRequest } from '../../store/photos.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-photos-user',
  templateUrl: './photos-user.component.html',
  styleUrls: ['./photos-user.component.sass']
})
export class PhotosUserComponent implements OnInit, OnDestroy {
  photos: Observable<Photo[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  removeLoading: Observable<boolean>;
  removeError: Observable<null | string>;
  user: Observable<null | User>;
  userSubscription!: Subscription;
  photoUserId = '';
  isMatchUsersId = false;

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) {
    this.photos = store.select(state => state.photos.photos);
    this.loading = store.select(state => state.photos.fetchUserPhotoLoading);
    this.error = store.select(state => state.photos.fetchUserPhotoError);
    this.user = store.select(state => state.users.user);
    this.removeLoading = store.select(state => state.photos.removeLoading);
    this.removeError = store.select(state => state.photos.removeError);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.photoUserId = params['id'];
      this.store.dispatch(fetchUserPhotosRequest({userId: this.photoUserId}));
    });
    this.userSubscription = this.user.subscribe(currentUser => {
      if (currentUser) {
        if (currentUser._id === this.photoUserId) {
          this.isMatchUsersId = true;
        }
      }
    });
  }

  openModalWindow(photoImage: string, photoTitle: string) {
    const dialogRef = this.dialog.open(ModalWindowComponent, {
      width: '60%',
      height: '80%',
      data: {image: photoImage, title: photoTitle},
    });
  }

  removePhoto(photoId: string) {
    this.store.dispatch(deletePhotoRequest({photoId}));
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }
}
